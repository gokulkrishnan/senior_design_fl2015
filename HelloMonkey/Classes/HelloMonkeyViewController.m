//
//  Copyright 2011 Twilio. All rights reserved.
//

#import "HelloMonkeyViewController.h"
#import "TwilioClient.h"
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OEAcousticModel.h>
@interface HelloMonkeyViewController()
{
    TCDevice* _phone;
    TCConnection* _connection;
    TCConnection* _connectionNew;
    dispatch_queue_t queue;
    NSString *otherNumber;
    NSString *addingName;
    NSString *addingNumber;
}
@end

@implementation HelloMonkeyViewController

@synthesize dialButton = _dialButton;
@synthesize hangupButton = _hangupButton;
@synthesize numberField = _numberField;


- (void)viewDidLoad
{
    didSayCall=FALSE;
    callByNumber=FALSE;
    isCallingByPeople= FALSE;
    phoneNumber=0;
    numberCounter=0;
    didAnswer = FALSE;
    actualNumber=@" ";
    otherNumber=@" ";
    callInProg=FALSE;
    counter=0;
    numToCall=[[NSMutableArray alloc]init];
    addingName=@" ";
    isServoConnected=FALSE;
    IPAddress=@"192.168.0.108";
    //button.frame=CGRectMake(50, 50, 50, 50);
    //[button setTitle:@"Say 'Contacts' or 'Number'" forState:UIControlStateNormal];
    [self initNetworkCommunication];
    //NSURL *SoundURL=[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"magic_bubbles" ofType:@"mp3"]];
    //AudioServicesCreateSystemSoundID((__bridge CFURLRef)SoundURL, &PlaySoundID);
    //AudioServicesPlaySystemSound(PlaySoundID);
    
    
    NSURL *url = [NSURL URLWithString:@"https://damp-ocean-2043.herokuapp.com/token?client=jenny"];
    NSError *error = nil;
    NSString *token = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if (token == nil) {
        NSLog(@"Error retrieving token: %@", [error localizedDescription]);
    } else {
        _phone = [[TCDevice alloc] initWithCapabilityToken:token delegate:self];
    }
    OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];
    
    words = [NSArray arrayWithObjects:@"CALL", @"PEOPLE", @"NUMBER", @"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0",@"ACCEPT", @"DECLINE",@"SPEAKERS",@"FRIENDS", @"STOP", nil];
    NSString *filepath = [self dataFilePath4];
    
    // NSMutableArray *array=[[NSMutableArray alloc]init];
    [words writeToFile:[self dataFilePath4] atomically:YES];
    NSLog(@"%@\n", filepath);

    NSString *name = @"NameIWantForMyLanguageModelFiles";
    NSError *err = [lmGenerator generateLanguageModelFromArray:words withFilesNamed:name forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to create a Spanish language model instead of an English one.
    
    NSString *lmPath = nil;
    NSString *dicPath = nil;
    
    if(err == nil) {
        
        lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
        dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
        
    } else {
        NSLog(@"Error: %@",[err localizedDescription]);
    }
    [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
    [[OEPocketsphinxController sharedInstance] startListeningWithLanguageModelAtPath:lmPath dictionaryAtPath:dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"] languageModelIsJSGF:NO]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to perform Spanish recognition instead of English.
    self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
    [self.openEarsEventsObserver setDelegate:self];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(aMethod)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Add a Contact" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor blackColor]];
    button.frame = CGRectMake(400.0, 850.0, 300.0, 40.0);
    [self.view addSubview:button];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self
               action:@selector(updateIP)
     forControlEvents:UIControlEventTouchUpInside];
    [button2 setTitle:@"Update IP Address" forState:UIControlStateNormal];
    [button2 setBackgroundColor:[UIColor blackColor]];
    button2.frame = CGRectMake(400.0, 900.0, 300.0, 40.0);
    [self.view addSubview:button2];
    
    UIButton *button5 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button5 addTarget:self
                action:@selector(restartApp)
      forControlEvents:UIControlEventTouchUpInside];
    [button5 setTitle:@"Restart" forState:UIControlStateNormal];
    [button5 setBackgroundColor:[UIColor blackColor]];
    button5.frame = CGRectMake(30.0, 800.0, 100.0, 40.0);
    [self.view addSubview:button5];
    
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 addTarget:self
                action:@selector(bMethod)
      forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:@"Show 'Friends'" forState:UIControlStateNormal];
    [button1 setBackgroundColor:[UIColor blackColor]];
    button1.frame = CGRectMake(400.0, 50.0, 300.0, 40.0);
    [self.view addSubview:button1];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button3 setBackgroundColor:[UIColor blackColor]];
    button3.frame = CGRectMake(30.0, 635.0, 290.0, 140.0);
    [self.view addSubview:button3];
    
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 655, 200, 20)];
    myLabel.text = @"Speakers Attached";
    myLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:myLabel];
    UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(50, 650, 0, 0)];
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    mySwitch.on=YES;
    [self.view addSubview:mySwitch];
    UILabel *myLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(110, 695, 200, 20)];
    myLabel1.text = @"Voice Recognition Active";
    myLabel1.textColor=[UIColor whiteColor];
    [self.view addSubview:myLabel1];
    UISwitch *mySwitch1 = [[UISwitch alloc] initWithFrame:CGRectMake(50, 690, 0, 0)];
    [mySwitch1 addTarget:self action:@selector(switchVR:) forControlEvents:UIControlEventValueChanged];
    mySwitch1.on=YES;
    [self.view addSubview:mySwitch1];
    UILabel *myLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(110, 735, 200, 20)];
    myLabel2.text = @"Speaker Position";
    myLabel2.textColor=[UIColor whiteColor];
    [self.view addSubview:myLabel2];
    UISwitch *mySwitch2 = [[UISwitch alloc] initWithFrame:CGRectMake(50, 730, 0, 0)];
    [mySwitch2 addTarget:self action:@selector(positionSpeakers:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:mySwitch2];
    
    

}
-(void) restartApp{
    exit(0);
}
-(void) updateIP{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter IP"
                                                    message:@"  "
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag=10;
    [alert show];
}
-(void) positionSpeakers:(id)sender{
    [self initNetworkCommunication];
    if([sender isOn]){
        long tag=7;
        NSString *response  = [NSString stringWithFormat:@"P%ld%@", tag , @"H"];
        NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
        [_outputStream write:[data bytes] maxLength:[data length]];
    }else{
        long tag=7;
        NSString *response  = [NSString stringWithFormat:@"P%ld%@", tag , @"L"];
        NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
        [_outputStream write:[data bytes] maxLength:[data length]];
    }
}
-(void)switchVR:(id)sender{
    if ([sender isOn]){
        OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];
        
        words = [NSArray arrayWithObjects:@"CALL", @"PEOPLE", @"NUMBER", @"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0",@"ACCEPT", @"DECLINE",@"SPEAKERS",@"FRIENDS", @"STOP", nil];
        NSString *filepath = [self dataFilePath4];
        
        // NSMutableArray *array=[[NSMutableArray alloc]init];
        [words writeToFile:[self dataFilePath4] atomically:YES];
        NSLog(@"%@\n", filepath);
        
        NSString *name = @"NameIWantForMyLanguageModelFiles";
        NSError *err = [lmGenerator generateLanguageModelFromArray:words withFilesNamed:name forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to create a Spanish language model instead of an English one.
        
        NSString *lmPath = nil;
        NSString *dicPath = nil;
        
        if(err == nil) {
            
            lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
            dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
            
        } else {
            NSLog(@"Error: %@",[err localizedDescription]);
        }
        [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
        [[OEPocketsphinxController sharedInstance] startListeningWithLanguageModelAtPath:lmPath dictionaryAtPath:dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"] languageModelIsJSGF:NO]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to perform Spanish recognition instead of English.
        self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
        [self.openEarsEventsObserver setDelegate:self];
    }else{
        [[OEPocketsphinxController sharedInstance] stopListening];
    }
}

- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        // Execute any code when the switch is ON
        NSLog(@"Switch is ON");
        isServoConnected=TRUE;
    } else{
        // Execute any code when the switch is OFF
        NSLog(@"Switch is OFF");
        isServoConnected=FALSE;
    }
}
- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)IPAddress, 7777, &readStream, &writeStream);
    _inputStream = (NSInputStream *)CFBridgingRelease(readStream);
    _outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    
    [_inputStream setDelegate:self];
    [_outputStream setDelegate:self];
    
    [_inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [_inputStream open];
    [_outputStream open];
}
-(void) ToggleSwitch{
    long tag=7;
NSString *response  = [NSString stringWithFormat:@"P%ld%@", tag , @"H"];
NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
[_outputStream write:[data bytes] maxLength:[data length]];
}
-(void) ToggleSwitch1{
    long tag=7;
    NSString *response  = [NSString stringWithFormat:@"P%ld%@", tag , @"L"];
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [_outputStream write:[data bytes] maxLength:[data length]];
}
-(void) ToggleSwitch2{
    long tag=7;
    NSString *response  = [NSString stringWithFormat:@"P%ld%@", tag , @"S"];
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [_outputStream write:[data bytes] maxLength:[data length]];
}
-(void) aMethod{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter Number"
                                                    message:@"  "
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Enter First Name"
                                                     message:@"  "
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    alert2.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert2 show];
    //read from file
    NSString *filepath = [self dataFilePath];
    NSArray *array = [[NSArray alloc]initWithContentsOfFile:filepath];
    NSLog(@"%@\n", array);
    NSLog(@"%@\n", filepath);
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if(alertView.tag==10){
            IPAddress = [alertView textFieldAtIndex:0].text;
            NSLog(@"%@", IPAddress);
        }else{
        NSString *input = [alertView textFieldAtIndex:0].text;
        // Insert whatever needs to be done with "name"
        
        if (counter%2==0){
            counter=counter+1;
            addingName=input;
            NSLog(@"%@", addingName);
            NSString *filepath = [self dataFilePath3];
           NSMutableArray *array = [[NSMutableArray alloc]initWithContentsOfFile:filepath];
           // NSMutableArray *array=[[NSMutableArray alloc]init];
            [array addObject: addingName];
             NSLog(@"%@", array);
            [array writeToFile:[self dataFilePath3] atomically:YES];
             NSLog(@"%@\n", filepath);
            
        }
        else{
            counter=counter+1;
            addingNumber=input;
            NSLog(@"%@", addingNumber);
            NSString *filepath = [self dataFilePath2];
            NSMutableArray *array = [[NSMutableArray alloc]initWithContentsOfFile:filepath];
            //NSMutableArray *array=[[NSMutableArray alloc]init];
            [array addObject: addingNumber];
            NSLog(@"%@", array);
            [array writeToFile:[self dataFilePath2] atomically:YES];
             NSLog(@"%@\n", filepath);
        }
        }
    }
}


-(NSString *) dataFilePath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"incomingNumber.plist"];
}
-(NSString *) dataFilePath3{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"contactnames.plist"];
}
-(NSString *) dataFilePath2{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"contactnumbers.plist"];
}

-(NSString *) dataFilePath4{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"words.plist"];
}
-(NSString *) dataFilePath5{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"updatedWords.plist"];
}

-(void) bMethod{
    NSLog(@"in bMethod");
    //write to file
    NSString *filepath = [self dataFilePath3];
    NSArray *array = [[NSArray alloc]initWithContentsOfFile:filepath];
    NSString *filepath2 = [self dataFilePath2];
      NSArray *array2 = [[NSArray alloc]initWithContentsOfFile:filepath2];
    NSLog(@"%@\n", array);
    NSLog(@"%@\n", filepath);
    int numberOfContacts= array.count;
    for (int i=0; i<numberOfContacts; i++){
        UILabel *label=[[UILabel alloc]initWithFrame: CGRectMake(400, 100+i*20, 100, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.text = array[i];
        [self.view addSubview:label];
        UILabel *label2=[[UILabel alloc]initWithFrame: CGRectMake(500, 100+i*20, 100, 20)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.textColor = [UIColor blackColor];
        label2.numberOfLines = 0;
        label2.lineBreakMode = UILineBreakModeWordWrap;
        label2.text = array2[i];
        [self.view addSubview:label2];
    }
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Limit to portrait for simplicity.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)viewDidUnload
{
	self.dialButton = nil;
	self.hangupButton = nil;
	self.numberField = nil;

	[super viewDidUnload];
}


-(IBAction)dialButtonPressed:(id)sender
{
    //actualNumber=otherNumber;
    //NSLog(@"here");
    //NSLog(@"%@", actualNumber);
    if (callInProg==FALSE) {
        if(isServoConnected){
            [self ToggleSwitch];
        }
    
        NSDictionary *params = @{@"To": self.numberField.text};
        if (actualNumber.length>5){
            NSLog(@"Here with actual Number");
            params = @{@"To": actualNumber};
        }
        NSLog(@"here2");
        callInProg=TRUE;
        NSLog(@"%@", params);
        _connection = [_phone connect:params delegate:nil];
        NSLog(@"here3");
    }

}

- (void)device:(TCDevice *)device didReceiveIncomingConnection:(TCConnection *)connection
{
    if (device.state == TCDeviceStateBusy) {
        [connection reject];
        NSLog(@"here");
    } else {
        _connectionNew=connection;
        NSString *path=[[NSBundle mainBundle] pathForResource:@"magic_bubbles" ofType:@"mp3"];
        AVAudioPlayer *theAudio = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error: NULL];
        [theAudio play];
       // [connection ignore];
       // _connectionNew=connection;
        NSDictionary *myDic=_connectionNew.parameters;
        NSString *myString=[myDic objectForKey:@"From"];
        NSLog(@"%@",myString);
        newNumber=myString;
        actualNumber=myString;
        otherNumber=myString;
        NSString *otherString = @"Incoming call from: ";
        NSString *newString=[otherString stringByAppendingString:myString];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:newString forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor blackColor]];
        button.frame = CGRectMake(50.0, 310.0, 300.0, 40.0);
        [self.view addSubview:button];
        otherNumber=actualNumber;
  
        NSMutableArray *array = [[NSMutableArray alloc]init];
        [array addObject:myString];
        [array writeToFile:[self dataFilePath] atomically:YES];
        
    [connection reject];
    }
}
- (void)deviceDidStartListeningForIncomingConnections:(TCDevice*)device
{
    NSLog(@"Device: %@ deviceDidStartListeningForIncomingConnections", device);
    NSLog(@"here2");

}

- (void)device:(TCDevice*)device didStopListeningForIncomingConnections:(NSError*)error
{
    NSLog(@"Device: %@ didStopListeningForIncomingConnections: %@", device, error);

}


-(IBAction)hangupButtonPressed:(id)sender
{
        [_connection disconnect];
    callInProg=FALSE;
    if(isServoConnected){
        [self ToggleSwitch1];
    }
}
-(void) callMethod{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Say 'People' or 'Number'" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor blackColor]];
    button.frame = CGRectMake(50.0, 310.0, 300.0, 40.0);
    [self.view addSubview:button];
    didSayCall=TRUE;
    
    
}
-(void)getNumber{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundColor:[UIColor blackColor]];
    button.frame = CGRectMake(500.0, 310.0, 300.0, 40.0);


    if ([lastword isEqualToString:@"NUMBER"]){
        [button setTitle:@"Calling By Number" forState:UIControlStateNormal];
        callByNumber=TRUE;
    }
    [self.view addSubview:button];
}

- (void) pocketsphinxDidReceiveHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore utteranceID:(NSString *)utteranceID {
    NSString *filepath6 = [self dataFilePath5];
    NSArray *updatedWordsArray = [[NSArray alloc]initWithContentsOfFile:filepath6];
    NSArray *justNames = [updatedWordsArray subarrayWithRange:NSMakeRange(0, [updatedWordsArray count]-18)];
    
    NSLog(@"The received hypothesis is %@ with a score of %@ and an ID of %@", hypothesis, recognitionScore, utteranceID);
     lastword=hypothesis;
    BOOL containsName=FALSE;
    NSString *personToCall = [[NSString alloc]init];
    for(int i=0; i<[justNames count]; i++){
        if ([hypothesis rangeOfString:justNames[i]].location != NSNotFound){
            containsName=TRUE;
            personToCall = justNames[i];
            
        }
    }
    NSLog(@"aboveif");
    NSLog(@"%hhd", containsName);
    NSLog(@"%hhd", isCallingByPeople);
    if(containsName && isCallingByPeople){
        NSLog(@"inif");
        //contact name uttered
        NSInteger index=[justNames indexOfObject:personToCall];
        NSLog(@"index found");
        NSString *filepath7 = [self dataFilePath2];
        NSArray *justNumbers = [[NSArray alloc]initWithContentsOfFile:filepath7];
        NSLog(@"file loaded");
        actualNumber=justNumbers[index];
        NSLog(@"atualNumber set");
        NSString *calling = @"Calling ";
        NSString *callingWho = [calling stringByAppendingString:personToCall];
        NSLog(@"callingWho setup");
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:callingWho forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor blackColor]];
        button.frame = CGRectMake(50.0, 310.0, 300.0, 40.0);
        [self.view addSubview:button];
        NSLog(@"before dialing");
        [self dialButtonPressed:button];
        NSLog(@"Dialing");
    }

    if ([hypothesis rangeOfString:@"CALL"].location != NSNotFound){
        [self callMethod];
        
    }
    if([hypothesis rangeOfString:@"STOP"].location !=NSNotFound){
        [_connection disconnect];
        if(isServoConnected){
        [self ToggleSwitch1];
        }
    }
    if([hypothesis rangeOfString:@"FRIENDS"].location != NSNotFound){
        [self bMethod];
    }
    if([hypothesis rangeOfString:@"SPEAKERS"].location != NSNotFound){
        [self ToggleSwitch2];
    }
    if([hypothesis rangeOfString:@"PEOPLE"].location !=NSNotFound||([hypothesis rangeOfString:@"NUMBER"].location !=NSNotFound && didSayCall)){
        if([hypothesis rangeOfString:@"NUMBER"].location !=NSNotFound){
        [self getNumber];
        }
        if ([hypothesis rangeOfString:@"PEOPLE"].location !=NSNotFound){
            [[OEPocketsphinxController sharedInstance] stopListening];
            NSString *filepath = [self dataFilePath3];
            NSArray *namesArray = [[NSArray alloc]initWithContentsOfFile:filepath];
            NSString *filepath2 = [self dataFilePath2];
            NSArray *numbersArray = [[NSArray alloc]initWithContentsOfFile:filepath2];
            NSLog(@"%@", namesArray);
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self
                       action:@selector(aMethod:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundColor:[UIColor blackColor]];
            button.frame = CGRectMake(50.0, 310.0, 300.0, 40.0);
            [button setTitle:@"Calling By People" forState:UIControlStateNormal];
            [self.view addSubview:button];
            [self bMethod];
            isCallingByPeople=TRUE;
            NSString *filepath4 = [self dataFilePath4];
            NSMutableArray *wordsArray = [[NSMutableArray alloc]initWithContentsOfFile:filepath4];
            
            int len=[namesArray count];
            for (int i=0; i<[namesArray count]; i++){
                int pos=len-i-1;
                NSLog(@"%@",namesArray[pos]);
               [wordsArray insertObject:[namesArray[pos] uppercaseString] atIndex:0];
            }
            NSLog(@"%@", wordsArray);
            NSString *filepath5 = [self dataFilePath5];
             [wordsArray writeToFile:[self dataFilePath5] atomically:YES];
            NSLog(@"%@\n", filepath5);
            OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];
            
            
            NSString *name = @"NameIWantForMyLanguageModelFiles";
            NSError *err = [lmGenerator generateLanguageModelFromArray:wordsArray withFilesNamed:name forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to create a Spanish language model instead of an English one.
            
            NSString *lmPath = nil;
            NSString *dicPath = nil;
            
            if(err == nil) {
                
                lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
                dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"NameIWantForMyLanguageModelFiles"];
                
            } else {
                NSLog(@"Error: %@",[err localizedDescription]);
            }
            [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
            [[OEPocketsphinxController sharedInstance] startListeningWithLanguageModelAtPath:lmPath dictionaryAtPath:dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"] languageModelIsJSGF:NO]; // Change "AcousticModelEnglish" to "AcousticModelSpanish" to perform Spanish recognition instead of English.
            self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
            [self.openEarsEventsObserver setDelegate:self];
        }
       
    }
    
    if([hypothesis isEqualToString: @"ACCEPT"]){
        didAnswer=TRUE;
        NSString *filepath = [self dataFilePath];
        NSLog(@"%@", filepath);
        NSArray *array = [[NSArray alloc]initWithContentsOfFile:filepath];
        actualNumber=array[0];
     //   actualNumber=newNumber;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Call in Progress" forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor blackColor]];
        button.frame = CGRectMake(50.0, 310.0, 300.0, 40.0);
        [self.view addSubview:button];
        [self dialButtonPressed:button];
        // Notifyall here
        //[self answermethod];
        
    }
    else if(callByNumber){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if(hypothesis.length<2){
        [numToCall addObject:hypothesis];
        NSLog(@"%@", numToCall);
        numberCounter=numberCounter+1;
        
        [button addTarget:self
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:hypothesis forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor blackColor]];
        button.frame = CGRectMake(200.0, 410.0, 300.0, 40.0);
        [self.view addSubview:button];
        }
        NSLog(@"numberCounter=%d",numberCounter);

        if (numberCounter==10){
                    actualNumber=@" ";
            for (int i=0; i<10; i++){
                NSString *temp=numToCall[i];
                NSLog(@"%@", temp);
                actualNumber=[actualNumber stringByAppendingString:temp];
            }
            [button setTitle: actualNumber forState: UIControlStateNormal];
            [self.view addSubview:button];
            NSLog(@"actualNumber%@", actualNumber);
            [self dialButtonPressed:button];
            //  _connection = [_phone connect:actualNumber delegate:nil];
        }
        
    }
   
}
-(void) answermethod{
    [_connectionNew accept];
}
/*typedef void(^now)(TCConnection myInt)	=	^{
    
    [connection accept];
};*/
- (void) pocketsphinxDidStartListening {
    NSLog(@"Pocketsphinx is now listening.");
}

- (void) pocketsphinxDidDetectSpeech {
    NSLog(@"Pocketsphinx has detected speech.");
}

- (void) pocketsphinxDidDetectFinishedSpeech {
    NSLog(@"Pocketsphinx has detected a period of silence, concluding an utterance.");
}

- (void) pocketsphinxDidStopListening {
    NSLog(@"Pocketsphinx has stopped listening.");
}

- (void) pocketsphinxDidSuspendRecognition {
    NSLog(@"Pocketsphinx has suspended recognition.");
}

- (void) pocketsphinxDidResumeRecognition {
    NSLog(@"Pocketsphinx has resumed recognition.");
}

- (void) pocketsphinxDidChangeLanguageModelToFile:(NSString *)newLanguageModelPathAsString andDictionary:(NSString *)newDictionaryPathAsString {
    NSLog(@"Pocketsphinx is now using the following language model: \n%@ and the following dictionary: %@",newLanguageModelPathAsString,newDictionaryPathAsString);
}

- (void) pocketSphinxContinuousSetupDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening setup wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) pocketSphinxContinuousTeardownDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening teardown wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) testRecognitionCompleted {
    NSLog(@"A test file that was submitted for recognition is now complete.");
}

@end
