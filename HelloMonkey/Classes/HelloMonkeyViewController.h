//
//  Copyright 2011 Twilio. All rights reserved.
//
 
#import <UIKit/UIKit.h>
#import <OpenEars/OEEventsObserver.h>
#import <AVFoundation/AVFoundation.h>
#import "TwilioClient.h"
#import "dispatch/dispatch.h"
#import <AudioToolbox/AudioToolbox.h>
@interface HelloMonkeyViewController : UIViewController <OEEventsObserverDelegate, NSStreamDelegate>
{
	UIButton* _dialButton;
	UIButton* _hangupButton;
    AVAudioPlayer *sound;
	UITextField* _numberField;
    NSString *lastword;
    BOOL didSayCall;
    int *phoneNumber;
    int numberCounter;
    int ten;
    BOOL callByNumber;
    BOOL didAnswer;
    BOOL isCallingByPeople;
    BOOL isServoConnected;
    BOOL callInProg;
    NSString *actualNumber;
    NSMutableArray *numToCall;
    NSString *newNumber;
    int counter;
    NSArray *words;
    SystemSoundID PlaySoundID;
    NSString *IPAddress;
    
}

@property (nonatomic, retain) IBOutlet UIButton* dialButton;
@property (nonatomic, retain) IBOutlet UIButton* hangupButton;
@property (nonatomic, retain) IBOutlet UITextField* numberField;
@property (strong, nonatomic) OEEventsObserver *openEarsEventsObserver;
@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;


-(IBAction)dialButtonPressed:(id)sender;
-(IBAction)hangupButtonPressed:(id)sender;

@end

