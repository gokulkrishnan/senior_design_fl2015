from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from Tkinter import *
import time

import RPi.GPIO as GPIO

#GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
#GPIO.setup(7, GPIO.OUT) ## Setup GPIO Pin 7 to OUT

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
pwm = GPIO.PWM(18, 100)
pwm.start(5)
#print sys.argv[1]

GPIO.setup(17,GPIO.OUT)
pwm2=GPIO.PWM(17,100)
pwm2.start(5)


class RaspberryLight(Protocol):
        def connectionMade(self):
                #self.transport.write("""connected""")
                self.factory.clients.append(self)
                print "clients are ", self.factory.clients

        def connectionLost(self, reason):
                print "connection lost ", self.factory.clients
                self.factory.clients.remove(self)


        def dataReceived(self, data):
                        msg = ""

                        if (data == 'P7H'):
                                msg = "Pin 7 is now High"
                                duty = float(180) / 10.0 + 2.5
                                print duty
                                pwm.ChangeDutyCycle(duty)
                                pwm2.ChangeDutyCycle(duty)

                        elif (data == 'P7L'):
                                msg = "Pin 7 is now Low"
                                duty = float(0) / 10.0 + 2.5
                                print duty
                                pwm.ChangeDutyCycle(duty)
                                pwm2.ChangeDutyCycle(duty)




                        print msg

factory = Factory()
factory.protocol = RaspberryLight
factory.clients = []

reactor.listenTCP(7777, factory)
print "RaspberryLight server started"
reactor.run()
